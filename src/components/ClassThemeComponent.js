import React, { Component } from 'react';
import { ThemeContext } from '../App';

export default class ClassThemeComponent extends Component {
  themeStyles(dark) {
    return {
      backgroundColor: dark ? '#000' : '#FFF',
      color: dark ? '#FFF' : '#000',
      fontSize: dark ? '12px' : '18px',
      height: '100vh',
      paddingRight: '5rem',
      paddingLeft: '5rem',
    };
  }

  render() {
    return (
      <ThemeContext.Consumer>
        {(darkTheme) => {
          return (
            <div style={this.themeStyles(darkTheme)}>
              <h3>Random title</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                labore et dolore magna aliqua. Sodales neque sodales ut etiam sit amet nisl purus. Etiam
                tempor orci eu lobortis elementum nibh tellus molestie nunc. Arcu cursus euismod quis viverra
                nibh cras. Arcu odio ut sem nulla pharetra diam. Posuere lorem ipsum dolor sit amet
                consectetur adipiscing elit. Hac habitasse platea dictumst quisque sagittis. Nulla facilisi
                morbi tempus iaculis urna id. Vulputate eu scelerisque felis imperdiet proin fermentum leo.
                Morbi tincidunt augue interdum velit. Aliquet sagittis id consectetur purus ut faucibus
                pulvinar elementum. Tristique senectus et netus et malesuada fames ac turpis. Arcu bibendum at
                varius vel pharetra vel turpis nunc eget. Turpis tincidunt id aliquet risus. Vel eros donec ac
                odio tempor orci dapibus. Neque vitae tempus quam pellentesque nec nam aliquam. Et odio
                pellentesque diam volutpat commodo sed egestas egestas fringilla. Nunc aliquet bibendum enim
                facilisis gravida neque. Eu facilisis sed odio morbi quis commodo.
              </p>
            </div>
          );
        }}
      </ThemeContext.Consumer>
    );
  }
}
