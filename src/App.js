import React, { useState } from 'react';
import ClassThemeComponent from './components/ClassThemeComponent';
import './App.css';

export const ThemeContext = React.createContext();

function App() {
  const [darkTheme, setDarkTheme] = useState(true);

  function toggleTheme() {
    setDarkTheme((prevDarkTheme) => !prevDarkTheme);
  }

  const buttonStyles = {
    color: 'red',
    fontWeight: 'bold',
    fontSize: '20px',
    marginTop: '20px',
    padding: '2rem',
  };

  return (
    <div className="App">
      <ThemeContext.Provider value={darkTheme}>
        <button onClick={toggleTheme} style={buttonStyles}>
          Toggle Theme
        </button>
        <ClassThemeComponent />
      </ThemeContext.Provider>
    </div>
  );
}

export default App;
